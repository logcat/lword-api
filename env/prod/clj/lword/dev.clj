(ns lword.dev
    (:require [environ.core :refer [env]]))

(def is-dev? (env :is-dev))

(if is-dev?
  (throw (Exception. (str "Production environment code is being loaded while the dev environment is active. "
                          "You likely have compiled class files lying around from an uberjar build. "
                          "Remove the target/ directory and try again."))))
