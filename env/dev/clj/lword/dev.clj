(ns lword.dev
  (:require [environ.core :refer [env]]))

(def is-dev? (env :is-dev))