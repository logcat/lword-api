(ns lword.server
  (:require [environ.core :refer [env]]
            [lword.dev :refer [is-dev?]]
            [compojure.core :refer [GET POST defroutes]]
            [compojure.route :refer [resources]]
            [ring.middleware.reload :as reload]
            [ring.middleware.defaults :refer [wrap-defaults api-defaults]]
            [ring.middleware.json :refer [wrap-json-response wrap-json-body]]
            [ring.middleware.cors :refer [wrap-cors]]
            [ring.adapter.jetty :refer [run-jetty]]
            [ring.util.response :refer [response]]
            [clj-http.client :as client])
  (:gen-class))

(defn wrap-external-api-request
  [do-request]
  (try
    (do-request)
    (catch Exception e "")))

(defn translate
  [word from to]
  (wrap-external-api-request
    #(let [response (client/get "http://mymemory.translated.net/api/get"
                                {:accept       :json
                                 :as           :json
                                 :query-params {"q"        word
                                                "langpair" (str from "|" to)}})]
      (->> response
           :body
           :responseData
           :translatedText))))

(defn image-url
  [word-eng]
  (wrap-external-api-request
    #(let [response (client/get "https://api.gettyimages.com/v3/search/images"
                                {:accept       :json
                                 :as           :json
                                 :headers      {"Api-Key" (env :getty-api-key)}
                                 :query-params {"phrase"     word-eng
                                                "sort_order" "best"
                                                "page_size"  1
                                                "fields"     "id,title,thumb,referral_destinations"}})]
      (->> response
           :body
           :images
           first
           :display_sizes
           first
           :uri))))

(defn audio-url
  [word lang]
  (wrap-external-api-request
    #(let [url (str "http://apifree.forvo.com/action/word-pronunciations/format/json/word/" word
                    "/language/" (.toLowerCase lang)
                    "/key/" (env :forvo-api-key))
           response (client/get url
                                {:accept :json
                                 :as     :json})]
      (->> response
           :body
           :items
           first
           :pathmp3))))

(defn best-word-for-image-lookup
  [word translation from to]
  (cond
    (.equalsIgnoreCase from "en") word
    (.equalsIgnoreCase to "en") translation
    :else (translate word from "en")))

(defn describe
  [{{:keys [word from to] :as body} :body}]
  (println body)
  (response
    (let [translation (translate word from to)
          audio (audio-url word from)
          image (image-url (best-word-for-image-lookup word translation from to))]
      (-> body
          (assoc :translation translation)
          (assoc :audio audio)
          (assoc :image image)))))

(defroutes routes
           (POST "/word" req (describe req)))

(def http-handler
  (let [handler (-> routes
                    (wrap-cors :access-control-allow-origin [#".*?"]
                               :access-control-allow-headers [:content-type]
                               :access-control-allow-methods [:get :put :post :delete])
                    (wrap-json-response)
                    (wrap-json-body {:keywords? true :bigdecimals? true}))]
    (if is-dev?
      (reload/wrap-reload handler)
      handler)))

(defn run-web-server [& [port]]
  (let [port (Integer. (or port (env :port) 10555))]
    (println (format "Starting web server on port %d." port))
    (run-jetty http-handler {:port port :join? false})))

(defn run [& [port]]
  (run-web-server port))

(defn -main [& [port]]
  (run port))