# Word description api

## Requirements
Provide api keys in .lein-env or as enviroment variables in production

## Build production
```
lein with-profile -dev,+prod uberjar
```

## Deploy
```
lein heroku deploy
```

## Sample request
```
curl -i -H "Content-Type: application/json" -X POST --data '{"word":"orava","from":"fi","to":"en"}' localhost:10555/word
```

or

```
curl -H "Content-Type: application/json" -X POST --data '{"word":"orava","from":"fi","to":"en"}' lword.herokuapp.com/word
```

