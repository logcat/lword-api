(defproject lword-api "0.1.0-SNAPSHOT"
  :description "LWord"

  :test-paths ["test/clj"]

  :dependencies [[org.clojure/clojure "1.7.0"]
                 [environ "1.0.1"]
                 [ring "1.4.0"]
                 [ring/ring-defaults "0.1.5"]
                 [ring/ring-json "0.4.0"]
                 [compojure "1.4.0"]
                 [clj-http "2.0.0"]
                 [ring-cors "0.1.7"]]

  :plugins [[lein-heroku "0.5.3"]]

  :uberjar-name "lword-api.jar"

  :heroku {:app-name      "lword"
           :jdk-version   "1.8"
           :include-files ["target/lword-api.jar"]
           :process-types {"web" "java -jar target/lword-api.jar"}}

  :profiles {:dev  {:source-paths ["src/clj" "env/dev/clj"]
                    :repl-options {:init-ns lword.server}
                    :env          {:is-dev true}}

             :prod {:source-paths ["src/clj" "env/prod/clj"]
                    :env          {:is-dev false
                                   :production true}
                    :omit-source  true
                    :aot          :all
                    :main         lword.server}})
